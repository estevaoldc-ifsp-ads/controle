// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCOFqJxkF57RDgotIvDenGTQzOZug9n0aw',
    authDomain: 'controle-if4.firebaseapp.com',
    projectId: 'controle-if4',
    storageBucket: 'controle-if4.appspot.com',
    messagingSenderId: '1058792691328',
    appId: '1:1058792691328:web:6cdaa40bb4798d528bf865',
    measurementId: 'G-62PT9CPDSE'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
